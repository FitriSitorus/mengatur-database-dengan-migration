@extends('layouts.master')

@section('title')
    Detail cast {{$cast->nama}}
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<h4>{{$cast->umur}}</h4>
<p>{{$cast->bio}}</p>

@endsection