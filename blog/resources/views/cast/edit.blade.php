@extends('layouts.master')

@section('title')
    Edit cast {{$cast->nama}}
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="title">Nama</label>
            <input type="text" value="{{$cast->nama}}" class="form-control" name="nama" id="title" placeholder="Masukkan Title">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="title">Umur</label>
            <input type="text" value="{{$cast->umur}}" class="form-control" name="umur" id="title" placeholder="Masukkan umur Anda">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="body">Bio</label>
            <textarea name="bio" class="from-control" cols="30" rows="10">{{$cast->bio}}</textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-success">Update</button>
    </form></form>
@endsection